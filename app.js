import { accounts } from './models/accounts.js';
import express from 'express';
import cookieParser from 'cookie-parser';
import authMiddleware from './middlewares/authMiddleware.js';
import {
  getHomePage,
  getLoginPage,
  getAboutPage,
} from './controllers/viewController.js';
import { login } from './controllers/authController.js';

const app = express();

app.set('view engine', 'ejs');

app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));
app.use('/static', express.static('assets'));
app.use(authMiddleware);

app.get('/', getHomePage);
app.get('/login', getLoginPage);
app.post('/login', login);
app.get('/about', getAboutPage);
app.post('/accounts', function (req, res) {
  const randomAccount = {
    id: '1234545',
    amount: '500 AZN',
  };
  accounts.push(randomAccount);
  res.redirect('/');
});

export default app;

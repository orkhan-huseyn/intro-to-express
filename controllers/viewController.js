import { accounts } from '../models/accounts.js';

export function getHomePage(req, res) {
  res.render('home', {
    userName: 'Rashad',
    accounts: accounts,
  });
}

export function getLoginPage(req, res) {
  res.render('login');
}

export function getAboutPage(req, res) {
  res.render('about');
}

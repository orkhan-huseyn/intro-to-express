import http from 'http';
import app from './app.js';

const server = http.createServer(app);

server.listen(8080, function () {
  console.log('HTTP server is running on port 8080');
});
